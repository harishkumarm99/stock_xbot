import datetime
import logging
import sys
import time
import urllib

import requests
from configparser import ConfigParser
from lib.lib_app.logger import *
from splinter import Browser


"""
    Function to compute date to milli-second epoch time

    Args :
        date : Date to be converted in milli-second

    Returns :
        Epoch time in milli-seocnd
    """


def format_date_to_epoch(date_str) -> str:
    logger.info("Date to Epoch time-conversion")
    d = [int(s) for s in date_str.split('/') if s.isdigit()]
    x = datetime.datetime(d[2], d[0], d[1]).timestamp()
    logger.debug("Date :"+date_str)
    logger.debug("Epoch date&time: ", x)
    return int(x)*1000


"""
    Function to get the redirect url

    Args :
        None

    Returns :
        Redirect url
    """


def get_redirect_url() -> str:
    # Grab configuration values.
    config = ConfigParser()
    config.read('configs/config.ini')

    CLIENT_ID = config.get('main', 'CLIENT_ID')
    REDIRECT_URI = config.get('main', 'REDIRECT_URI')
    CREDENTIALS_PATH = config.get('main', 'JSON_PATH')
    ACCOUNT_NUMBER = config.get('main', 'ACCOUNT_NUMBER')
    CHROMEDRIVER_PATH = config.get('main', 'CHROMEDRIVER_PATH')
    USERNAME = config.get('main', 'USERNAME')
    PASSWORD = config.get('main', 'PASSWORD')
    MYANSWER = config.get('main', 'MYANSWER')
    logger.info("TDA login extension")

    logger.info("Getting the redirect url through browser automation")
    executable_path = {'executable_path': CHROMEDRIVER_PATH}
    # creating the browser
    browser = Browser('chrome', **executable_path, headless=True)
    method = 'GET'
    url = 'https://auth.tdameritrade.com/auth?'
    client_code = CLIENT_ID + '@AMER.OAUTHAP'
    payload = {'response_type': 'code',
               'redirect_uri': REDIRECT_URI, 'client_id': client_code}

    # build the url
    built_url = requests.Request(method, url, params=payload).prepare()
    built_url = built_url.url
    logger.info("url: "+built_url)
    browser.visit(built_url)

    # Fill Out the Form
    payload_fill = {'username': USERNAME,
                    'password': PASSWORD}
    browser.find_by_id('username0').first.fill(payload_fill['username'])
    browser.find_by_id('password').first.fill(payload_fill['password'])
    browser.find_by_id('accept').first.click()
    logger.debug(
        "Sleeping for 1 sec, to determine if there is 2 factor auth..")
    time.sleep(1)

    if (len(browser.find_by_text('Get Code via Text Message')) == 1):
        logger.debug("2factor auth is enabled..")
        # Get the Text Message Box
        browser.find_by_text('Can\'t get the text message?').first.click()
        # Get the Answer Box
        browser.find_by_value("Answer a security question").first.click()
        # Answer the Security Questions.
        browser.find_by_id('secretquestion0').first.fill(MYANSWER)
        
        # Submit results
        browser.find_by_id('accept').first.click()
        # Sleep and click Accept Terms for Trust this device..
        print("Sleeping for 1 sec, clicking trust this device..")
        time.sleep(1)
        browser.find_by_xpath('//*[@id="stepup_trustthisdevice0"]/div[1]/label').click()
        browser.find_by_id('accept').first.click()

        # Sleep and click Accept Terms.
        print("Sleeping for 1 sec, clicking accept..")
        time.sleep(1)

    browser.find_by_id('accept').first.click()
    # give it a second, then grab the url
    logger.debug("Sleeping for 1 sec, to get the url..")
    time.sleep(1)
    new_url = browser.url
    return new_url
