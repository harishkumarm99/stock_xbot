import asyncio
import json
import threading
from typing import Dict, List
import requests
import paho.mqtt.client as mqtt
import paho.mqtt.publish as mqtt_publish
import pandas as pd
import websockets
from typing import Dict
from typing import List
from typing import Optional
from typing import Any

import logging
from lib.lib_app.logger import *
from lib.lib_ext.ext_td_fields import (CSV_FIELD_KEYS, CSV_FIELD_KEYS_LEVEL_2,
                                       STREAM_FIELD_IDS)
from lib.lib_ext.helper import format_date_to_epoch, get_redirect_url
from td.client import TDClient, TDStreamerClient


def aggregate_minute_data(read_thd, key, new_minute_data, agg_freq=5):
    if (key not in read_thd.agg_data or read_thd.agg_data[key]['index'] == 0):
        new_content = {'open': new_minute_data['open'], 'high': new_minute_data['high'],
                       'low': new_minute_data['low'], 'close': new_minute_data['close'],
                       'volume': new_minute_data['volume'], 'datetime': new_minute_data['datetime']}
        read_thd.agg_data[key] = {'index': 1, 'content': new_content}
    else:
        read_thd.agg_data[key]['content']['low'] = min(
            read_thd.agg_data[key]['content']['low'], new_minute_data['low'])
        read_thd.agg_data[key]['content']['high'] = max(
            read_thd.agg_data[key]['content']['high'], new_minute_data['high'])
        read_thd.agg_data[key]['content']['volume'] += new_minute_data['volume']
        read_thd.agg_data[key]['index'] += 1

        # At the end of the agg_freq iteration, return agg data
        if (read_thd.agg_data[key]['index'] == agg_freq):
            read_thd.agg_data[key]['content']['close'] = new_minute_data['close']
            read_thd.agg_data[key]['content']['datetime'] = new_minute_data['datetime']
            read_thd.agg_data[key]['index'] = 0
            logger.info(f"Aggregated minute data for {key}")
            return read_thd.agg_data[key]['content']

    logger.info(
        f"Aggregating minute data for {key} - index {read_thd.agg_data[key]['index']}")
    return {}


def append_state_data(thd, key, new_entry):
    logger.debug(len(thd.state[key]))
    thd.state[key].append(new_entry)
    logger.debug(len(thd.state[key]))
    thd.state[key] = thd.state[key][1:]
    logger.debug(len(thd.state[key]))


def send_to_analyze(thd, key):
    logger.info("sending read_stream to analyze")
    data = {"key": key, "value": thd.state[key]}
    mqtt_publish.single(topic="read_stream",
                        payload=json.dumps(data), hostname="localhost")


def process_msg(message_decoded):
    read = threading.current_thread()
    if ('notify' in message_decoded):
        return

    if ('data' in message_decoded):
        for msg in message_decoded['data']:
            if (msg['service'] == 'CHART_EQUITY'):
                for ticker in msg['content']:
                    key = ticker['key']
                    d = ticker['7']
                    o, h, l, c, v = ticker['1'], ticker['2'], ticker['3'], ticker['4'], ticker['5']
                    new_content = {'open': o, 'high': h, 'low': l,
                                   'close': c, 'volume': v, 'datetime': d}
                    for (i, k) in enumerate(read.state):
                        if (k[0] == key):
                            agg_content = aggregate_minute_data(
                                read, k, new_content, 5)
                            if (agg_content):
                                append_state_data(read, k, agg_content)
                                send_to_analyze(read, k)

    logger.info(message_decoded)


"""
  TDA authentication extended class
"""


class extTDStreamerClient(TDStreamerClient):
    def __init__(self, websocket_url: str, user_principal_data: dict, credentials: dict, loop: asyncio.AbstractEventLoop = None) -> None:
        """Initalizes the Streaming Client.

        Initalizes the Client Object and defines different components that will be needed to
        make a connection with the TD Streaming API.

        Arguments:
        ----
        websocket_url {str} -- The websocket URL that is returned from a Get_User_Prinicpals Request.

        user_principal_data {dict} -- The data that was returned from the "Get_User_Principals" request. 
            Contains the info need for the account info.

        credentials {dict} -- A credentials dictionary that is created from the "create_streaming_session"
            method.

        Usage:
        ----

            >>> td_session = TDClient(
                client_id='<CLIENT_ID>',
                redirect_uri='<REDIRECT_URI>',
                credentials_path='<CREDENTIALS_PATH>'
            )
            >>> td_session.login()
            >>> td_stream_session = td_session.create_streaming_session()

        """

        self.websocket_url = "wss://{}/ws".format(websocket_url)
        self.credentials = credentials
        self.user_principal_data = user_principal_data
        self.connection: websockets.WebSocketClientProtocol = None
        self.file_stream_level_1: io.TextIOWrapper = None
        self.file_stream_level_2: io.TextIOWrapper = None

        # this will hold all of our requests
        self.data_requests = {"requests": []}

        # this will house all of our field numebrs and keys so that way the user can use names to define the fields they want.
        self.fields_ids_dictionary = STREAM_FIELD_IDS
        self.fields_keys_write = CSV_FIELD_KEYS
        self.fields_keys_write_level_2 = CSV_FIELD_KEYS_LEVEL_2
        self.approved_writes_level_1 = list(self.fields_keys_write.keys())
        self.approved_writes_level_2 = list(
            self.fields_keys_write_level_2.keys())

        self.print_to_console = True
        self.write_flag = False
        self.streaming_in_progress = False
        try:
            self.loop = loop or asyncio.get_event_loop()
        except websockets.WebSocketException:
            self.loop = asyncio.new_event_loop()
            asyncio.set_event_loop(self.loop)

        self.unsubscribe_count = 0

    async def _receive_message_ext(self, return_value: bool = False, process_msg=None) -> dict:
        """Recieves and processes the messages as needed.

        Keyword Arguments:
        ----
        return_value {bool} -- Specifies whether the messages should be returned
            back to the calling function or not. (default: {False})

        Returns:
        ----
        {dict} -- A python dictionary
        """
        logger.info("asyncio _receive_message_ext")
        # Keep going until cancelled.
        while True:

            try:

                # Grab the Message
                message = await self.connection.recv()

                # Parse Message
                message_decoded = await self._parse_json_message(message=message)

                # Write the data if needed.
                if self.write_flag:
                    try:
                        await self._write_to_csv(data=message_decoded)
                    except:
                        print('Could not write content to CSV file, closing stream')
                        # await asyncio.get_running_loop().stop()
                        await self.close_stream()
                        break

                if process_msg:
                    try:
                        process_msg(message_decoded)
                        await asyncio.sleep(1)
                    except:
                        logger.error("Prcess stream data exception")

                if return_value:
                    return message_decoded

                elif self.print_to_console:
                    logger.debug(message_decoded)
                    # print('='*20)
                    # print('Message Recieved:')
                    # print('')
                    # print(message_decoded)
                    # print('-'*20)
                    # print('')

            except websockets.exceptions.ConnectionClosed:

                # stop the connection if there is an error.
                await self.close_stream()
                break

    async def _send_message(self, message: str):
        """Sends a message to webSocket server

        Arguments:
        ----
        message {str} -- The JSON string with the
            data streaming service subscription.
        """
        logger.info("asyncio _send")
        await self.connection.send(message)

    def stream_ext(self, print_to_console: bool = True) -> None:
        """Starts the stream and prints the output to the console.

        Initalizes the stream by building a login request, starting 
        an event loop, creating a connection, passing through the 
        requests, and keeping the loop running.

        Keyword Arguments:
        ----
        print_to_console {bool} -- Specifies whether the content is to be printed
            to the console or not. (default: {True})
        """
        try:
            # Streaming in progress
            self.streaming_in_progress = True
            
            # Print it to the console.
            self.print_to_console = print_to_console

            # Connect to the Websocket.
            self.loop.run_until_complete(self._connect(pipeline_start=True))

            # Send the Request.
            asyncio.ensure_future(
                self._send_message(self._build_data_request()))

            if (threading.current_thread().name != 'MainThread'):
                # Start Recieving Messages. Come here if we are running in a threadpool
                asyncio.ensure_future(self._receive_message_ext(
                    return_value=False, process_msg=process_msg))
            else:
                # Keep the Loop going, until an exception is reached. Come here if we are in MainThread
                asyncio.ensure_future(
                    self._receive_message(return_value=False))
                self.loop.run_forever()
        except:
            logger.error("Fatal exception")


class extTDClient(TDClient):
    def get_price_history_ext(self, symbol: str, period_type: str = None, period=None, start_date: str = None, end_date: str = None,
                              frequency_type: str = None, frequency: str = None, extended_hours: bool = True) -> str:
        """
        start_date, end_data format : mm/dd/yyyy
        """
        logger.info("Getting history of stock price")
        logger.debug("Stock symbol: "+symbol)
        logger.debug(
            "Period Type: "+period_type) if period_type else logger.debug('period_type: None')
        logger.debug(
            'Period: '+period) if period else logger.debug('Period: None')
        logger.debug('Start Date: '+start_date)
        logger.debug('End Date: '+end_date)
        logger.debug("Frequency Type: "+frequency_type)
        logger.debug("Frequency: "+str(frequency)
                     ) if frequency else logger.debug('frequency: None')
        logger.debug('Extended hours trading: ' + str(extended_hours))

        if (start_date):
            start_date = format_date_to_epoch(start_date)
        if (end_date):
            end_date = format_date_to_epoch(end_date)

        api_response = self.get_price_history(symbol, period_type, period, start_date, end_date,
                                              frequency_type, frequency, extended_hours)
        if (api_response):
          #  df = pd.DataFrame(api_response['candles'])
          #  df['datetime'] = pd.to_datetime(df['datetime'], unit='ms')
            #logger.debug("Price history response "+df)
            return api_response['candles']
        else:
            logger.error("No response from TDA for get_price_history()")

    def get_quotes_ext(self, instruments: List) -> Dict:
        """Logs the user into the TD Ameritrade API.
        Automates the login process and populates the credentials file with access_token
        Returns:

            bool -- Specifies whether it was successful or not.
        """
        return self.get_quotes(instruments)

    def tda_login_ext(self) -> bool:
        """TDA login extension
        Automates the login process and populates the credentials file with access_token
        Returns:

            bool -- Specifies whether it was successful
        """
        logger_init()

        # if caching is enabled then attempt silent authentication.
        if self.config['cache_state'] and self._silent_sso():
            self.authstate = 'Authenticated'
            logger.info("TDA already been authenticated successfully")
            return True
        else:
            logger.error("Error authenticat to TDA")

        redirect_url = get_redirect_url()
        # store the redirect URL
        self._redirect_code = redirect_url

        # this will complete the final part of the authentication process.
        self.grab_access_token()
        self.authstate = 'Authenticated'
        logger.info("TDA authenticated")
        logger.info("TDA authenticated successfully")
        return True

    def create_streaming_session_ext(self, loop: asyncio.AbstractEventLoop = None) -> extTDStreamerClient:
        """Creates a new streaming session with the TD API.

        Grab the token to authenticate a stream session, builds
        the credentials payload, and initalizes a new instance
        of the TDStream client.

        Usage:
        ----
            >>> td_session = TDClient(
                client_id='<CLIENT_ID>',
                redirect_uri='<REDIRECT_URI>',
                credentials_path='<CREDENTIALS_PATH>'
            )
            >>> td_session.login()
            >>> td_stream_session = td_session.create_streaming_session()

        Returns:
        ----
        TDStreamerClient -- A new instance of a Stream Client that can be
            used to subscribe to different streaming services.
        """

        # Grab the Streamer Info.
        userPrincipalsResponse = self.get_user_principals(
            fields=['streamerConnectionInfo', 'streamerSubscriptionKeys', 'preferences', 'surrogateIds'])

        # Grab the timestampe.
        tokenTimeStamp = userPrincipalsResponse['streamerInfo']['tokenTimestamp']

        # Grab socket
        socket_url = userPrincipalsResponse['streamerInfo']['streamerSocketUrl']

        # Parse the token timestamp.
        tokenTimeStampAsMs = self._create_token_timestamp(
            token_timestamp=tokenTimeStamp)

        # Define our Credentials Dictionary used for authentication.
        credentials = {
            "userid": userPrincipalsResponse['accounts'][0]['accountId'],
            "token": userPrincipalsResponse['streamerInfo']['token'],
            "company": userPrincipalsResponse['accounts'][0]['company'],
            "segment": userPrincipalsResponse['accounts'][0]['segment'],
            "cddomain": userPrincipalsResponse['accounts'][0]['accountCdDomainId'],
            "usergroup": userPrincipalsResponse['streamerInfo']['userGroup'],
            "accesslevel": userPrincipalsResponse['streamerInfo']['accessLevel'],
            "authorized": "Y",
            "timestamp": tokenTimeStampAsMs,
            "appid": userPrincipalsResponse['streamerInfo']['appId'],
            "acl": userPrincipalsResponse['streamerInfo']['acl']
        }

        # Create the session
        streaming_session = extTDStreamerClient(
            websocket_url=socket_url,
            user_principal_data=userPrincipalsResponse,
            credentials=credentials
            # loop=loop
        )

        return streaming_session

    def _make_request_ext(self, method: str, endpoint: str, mode: str = None, params: dict = None, data: dict = None, json: str = None, 
                        order_details: bool = False) -> Any:
        """Handles all the requests in the library.

        A central function used to handle all the requests made in the library,
        this function handles building the URL, defining Content-Type, passing
        through payloads, and handling any errors that may arise during the request.

        Arguments:
        ----
        method: The Request method, can be one of the
            following: ['get','post','put','delete','patch']
        
        endpoint: The API URL endpoint, example is 'quotes'

        mode: The content-type mode, can be one of the
            following: ['form','json']
        
        params: The URL params for the request.
        
        data: A data payload for a request.

        json: A json data payload for a request

        Returns:
        ----
        A Dictionary object containing the JSON values.            
        """

        url = self._api_endpoint(endpoint=endpoint)
        headers = self._headers(mode=mode)

        # Make sure the token is valid if it's not a Token API call.
        if endpoint != self.config['token_endpoint']:
            self._token_validation()
        elif endpoint == self.config['token_endpoint']:
            del headers['Authorization']

        # Handle the request.
        if method == 'get':   
            response = requests.get(url=url, headers=headers, params=params, data=data, json=json, verify=True)
        elif method == 'post':            
            response = requests.post(url=url, headers=headers, params=params, data=data, json=json, verify=True)
        elif method == 'put':
            response = requests.put(url=url, headers=headers, params=params, data=data, json=json, verify=True)
        elif method == 'delete':
            response = requests.delete(url=url, headers=headers, params=params, data=data, json=json, verify=True)
        elif method == 'patch':
            response = requests.patch(url=url, headers=headers, params=params, data=data, json=json, verify=True)

        # grab the status code
        status_code = response.status_code

        # grab the response headers.
        response_headers = response.headers

        # Grab the order id, if it exists.
        if 'Location' in response_headers:
            order_id = response_headers['Location'].split('orders/')[1]
        else:
            order_id = ''

        if status_code in (200, 201):

            if order_details:

                response_dict = {
                    'order_id':order_id,
                    'headers':response_headers,
                    'content':response.content,
                    'status_code':status_code,
                    'request_body':response.request.body,
                    'request_method':response.request.method
                }

                return response_dict

            elif response_headers['Content-Type'] in ('application/json;charset=UTF-8','application/json'):
                return response.json()



        elif status_code in (401, 400, 403, 415, 500):
            print('-'*80)
            print("BAD REQUEST - STATUS CODE: {}".format(status_code))
            print("RESPONSE URL: {}".format(response.url))
            print("RESPONSE HEADERS: {}".format(response.headers))
            print("RESPONSE PARAMS: {}".format(response.links))
            print("RESPONSE TEXT: {}".format(response.text))
            print('-'*80)

    def place_order_ext(self, account: str, order: dict) -> dict:
        """Places an order for a specific account.

        Documentation:
        ----
        https://developer.tdameritrade.com/account-access/apis/delete/accounts/%7BaccountId%7D/orders/%7BorderId%7D-0

        Arguments:
        ----
        account {str} -- The account number that you want to place the order for.

        order {dict} -- The order payload.

        Usage:
        ----
            >>> SessionObject.place_order(account='MyAccountID', order={'orderKey':'OrderValue'})
        
        Returns:
        ----
        {dict} -- A response dicitonary.
        """
        order = json.dumps(dict(order._grab_order()))
        # make the request
        endpoint = 'accounts/{}/orders'.format(account)
        return self._make_request_ext(method='post', endpoint=endpoint, mode='json', json=order, order_details=True)

