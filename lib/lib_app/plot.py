import pdb

import finplot as fplt
import matplotlib.dates as mpl_dates
import matplotlib.gridspec as grid_spec
import numpy as np
from matplotlib import dates as mpl_dates
from matplotlib import pyplot as plt
from matplotlib.dates import DateFormatter
from matplotlib.pyplot import plot, show
from matplotlib.widgets import Button, Cursor
from pandas import DataFrame, Series

#date_format = mpl_dates.DateFormatter("%b-%Y")
#date_format_full = matplotlib.dates.DateFormatter("%d-%b-%Y")
#def fmt(x, y): return "{}, {:.5g}".format(date_format_full(x), y)

"""
    Function to draw OHLC candless with indicators

    Args :
        df : Pandas DataFrame which contains ['date', 'open', 'high', 'low', 'close', 'volume'] columns
        df : Overlap indicator 1 to draw in candles
        df : Overlap indicator 2 to draw in candles
        df : Overlap indicator 3 to draw in candles
        df : Bottom 1 indicator
        df : Bottom 2 indicator
        df : Bottom 3 indicator

    Returns :
        none
    """
# https://github.com/highfestiva/finplot


def plot_ohlc_3x3(ohlc_df: DataFrame, overlap_df1: DataFrame, overlap_df2: DataFrame, overlap_df3: DataFrame,
                  bottom_df1: DataFrame, bottom_df2: DataFrame, bottom_df3: DataFrame):

    if ((not ohlc_df.empty) and (not bottom_df1.empty) and (not bottom_df1.empty) and (not bottom_df1.empty)):
        # create four plots
        plt1, plt2, plt3, plt4 = fplt.create_plot(
            ohlc_df.instrument_name, rows=4)
    elif ((not ohlc_df.empty) and (not bottom_df1.empty) and (not bottom_df2.empty)):
        # create three plots
        plt1, plt2, plt3 = fplt.create_plot(ohlc_df.instrument_name, rows=3)
    elif ((not ohlc_df.empty) and (not bottom_df1.empty)):
        # create  two plots
        plt1, plt2 = fplt.create_plot(ohlc_df.instrument_name, rows=2)
    elif (not ohlc_df.empty):
        # create  one plot
        plt1 = fplt.create_plot(ohlc_df.instrument_name, rows=1)

    if (not ohlc_df.empty):
        # plot candle sticks
        candles = ohlc_df[['datetime', 'open', 'close', 'high', 'low']]
        fplt.candlestick_ochl(candles, ax=plt1)
        # overlay volume on the top plot
        volumes = ohlc_df[['datetime', 'open', 'close', 'volume']]
        fplt.volume_ocv(volumes, ax=plt1.overlay())

    if (not overlap_df1.empty):
       # pdb.set_trace()
        # put an overlay plot
        fplt.plot(ohlc_df['datetime'], overlap_df1,
                  ax=plt1, legend=overlap_df1.name)

    if (not overlap_df2.empty):
        # put an overlay plot
        fplt.plot(ohlc_df['datetime'], overlap_df2,
                  ax=plt1, legend=overlap_df2.name)

    if (not overlap_df3.empty):
        # put an overlay plot
        fplt.plot(ohlc_df['datetime'], overlap_df3,
                  ax=plt1, legend=overlap_df3.name)

    # place some dumb markers on low wicks
    # lo_wicks = ohlc_df[['open', 'close']].T.min() - ohlc_df['low']
    # ohlc_df.loc[(lo_wicks > lo_wicks.quantile(0.99)),
    #             'marker'] = ohlc_df['low']
    # fplt.plot(ohlc_df['datetime'], ohlc_df['marker'], ax=ax,
    #           color='#4a5', style='^', legend='dumb mark')

    if (not bottom_df1.empty):
        for col in bottom_df1.columns:
            fplt.plot(ohlc_df['datetime'], bottom_df1[col],
                      ax=plt2, legend=col)

    if (not bottom_df2.empty):
        for col in bottom_df2.columns:
            fplt.plot(ohlc_df['datetime'], bottom_df2[col],
                      ax=plt3, legend=col)

    if (not bottom_df3.empty):
        for col in bottom_df3.columns:
            fplt.plot(ohlc_df['datetime'], bottom_df3[col],
                      ax=plt4, legend=col)

    # restore view (X-position and zoom) if we ever run this example again
  #  fplt.autoviewrestore()
    fplt.show()

    return
