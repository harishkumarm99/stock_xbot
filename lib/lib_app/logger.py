import glob
import os

from loguru import *


"""
    Function to initialzie the message logger
    Args :
        None
"""


def logger_init():
    logger.info("Logger initializion")
    files = glob.glob("logs/file*.log", recursive=True)
    for file in files:
        try:
            os.remove(file)
        except OSError as e:
            logger.error("Error: %s : %s" % (file, e.strerror))

    logger.add("logs/file_{time}.log", rotation="500 MB")
    # Automatically rotate too big file
    # logger.add("logs/file_{time}", rotation="500 MB")
    # New file is created each day at noon
    # logger.add("logs/file_{time}", rotation="12:00")
    # # Once the file is too old, it's rotated
    # logger.add("logs/file_{time}", rotation="1 week")
    # # Cleanup after some time
    # logger.add("logs/file_{time}", retention="10 days")
    # logger.add("logs/file_{time}", compression="zip")    # Save some loved space
    # logger.add(
    #     "logs/file_{time}.log", format="{time: YYYY-MM-DD at HH: mm: ss} | {level} | {message}")


def logger_init_test():
    logger.info("Test Suites Logger initializion")
    files = glob.glob("logs/test*.log", recursive=True)
    # for f in files:
    #     try:
    #         os.remove(f)
    #     except OSError as e:
    #         logger.error("Error: %s : %s" % (f, e.strerror))

    logger.add("logs/test_{time}.log", rotation="500 MB", format="{message}")


def logger_init_report():
    logger.info("Test Report Logger initializion")
    files = glob.glob("logs/report*.log", recursive=True)
    for f in files:
        try:
            os.remove(f)
        except OSError as e:
            logger.error("Error: %s : %s" % (f, e.strerror))

    logger.add("logs/report_{time}.log", format="{message}")
