import datetime as dt
import commentjson
import os
import sys
import time

import pandas as pd
import schedule
from apscheduler.schedulers.background import BlockingScheduler

from buy_sell.exec_order import *
from lib.lib_app.logger import *

##################################
# Use case  
# ---------
# 1. Earnings AMC - After Market Close  => Market Order Buy at 12:59:XX and Limit Order Sell after close with 
#       1) tickerCode & date & time
#       2) quantity | percentage | amount 
#       3) stategy = enter_exit | exit_pending
#       4) lmt_price | enter_exit_percentage - price to trigger sell order
#       5) partial_fill_retry & partial_fill_wait_time
#       6) Failsafe - Add a seperate sell order just at ER time, just in case if we did not hit our target profit
#
# 2. Earnings BMO - Before Market Open => Market Order Buy at 12:59:XX and schedule limit order sell the following day before ER
#   Part 1 : Market Order buy at 12:59pm
#       1) tickerCode & date & time
#       2) quantity | percentage | amount 
#       3) enter_or_exit = enter
#   Part 2 : Limit Order sell at/after 4am the following day
#       1) tickerCode & date & time
#       2) quantity
#       3) enter_or_exit = exit
#       4) lmt_price - price to trigger sell order
#       5) partial_fill_retry & partial_fill_wait_time
#       6) Failsafe - Add a seperate sell order just at ER time, just in case if we did not hit our target profit (lmt_price)

# Read Limit order sell information
def get_order_json_data():
    print("Fetch the order data './configs/order.json' ")
    f = open('./configs/order.json', "r")
    data = commentjson.loads(f.read())
    f.close()
    return data

# Print the schedule object
def validate_print_schd_object(stocks_obj: dict):
    print("***********  Scheduling object  *********** ")
    sched_dict = json.loads(stocks_obj)

    stocks_data = sched_dict['stocks_info']
     
    print(f"Account Number\t\t: {sched_dict['account_num']}")

    if (stocks_data['strategy_name'] == 'enter_exit'):
        if (stocks_data['tickerDesc'] != ""):
            print(f"Stock name\t\t: {stocks_data['tickerDesc']}")

        if (stocks_data['strategy_name'] != ""):
            print(f"Stock ticker            : {stocks_data['tickerCode']}")
        else:
            print("Error: Stocker ticker is mandatory for strategy")
            return

        print("Strategy Name           : Enter and Exit")

        if (stocks_data['asset_type'] != ""):
            print(f"Stock type              : {stocks_data['asset_type']}")
        else:
            print("Error: Assert type is mandatory for the strategy")
            return

        if (stocks_data['date'] != ""):
            print(f"Stock buy/sell date     : {stocks_data['date']}")
        else:
            print("Error: Assert type is mandatory for the strategy")
            return
          
        if (stocks_data['time'] != ""):
            print(f"Stock buy/sell time     : {stocks_data['time']}")
        else:
            print("Stock buy/sell time      : Now")

        if (stocks_data['stock_quantity'] != ""):
            print(f"Stock quantity          : {stocks_data['stock_quantity']}")
        else:
            print("Error: Stock quantity is NULL")
            return

        if (stocks_data['order_type'] == "lmt"):
            stocks_data['partial_fill_retry'] = stocks_data.get('partial_fill_retry', 0)          
            print(f"Partial retry           : {stocks_data['partial_fill_retry']}")
            
            stocks_data['partial_fill_wait_time'] = stocks_data.get('partial_fill_wait_time', 0)          
            print(f"Partial fill wait-time  : {stocks_data['partial_fill_wait_time']}")

        if (stocks_data['order_type'] != ""):
            print(f"Order type              : {stocks_data['order_type']}")
        else:
            print("Error: Stock Order type is NULL")
            return

        if (stocks_data['enter_exit_percentage'] != ""):
            print(f"Profit %                : {stocks_data['enter_exit_percentage']}")
        else:
            print("Error: Stock enter_exit_percentage is NULL")
            return
    print("                                         ")
    return


# Called by scheduler when timer expires
def schedule_job(text):
    print("Scheduling Job")
    sched_dict = json.loads(text)
    exec_order(sched, sched_dict)
   # print(sched_dict)

def validate_data(order_data: dict) -> dict:
    # Grab configuration values.
    config = ConfigParser()
    config.read('configs/config.ini')

    CLIENT_ID = config.get('main', 'CLIENT_ID')
    REDIRECT_URI = config.get('main', 'REDIRECT_URI')
    CREDENTIALS_PATH = config.get('main', 'JSON_PATH')
    ACCOUNT_NUMBER = config.get('main', 'ACCOUNT_NUMBER')
    
    # Initalize the robot.
    trading_robot = PyRobot(
        client_id=CLIENT_ID,
        redirect_uri=REDIRECT_URI,
        credentials_path=CREDENTIALS_PATH,
        trading_account=ACCOUNT_NUMBER,
        paper_trading=False
    )

    account_list = trading_robot.session.get_accounts()
    account_dict = {}
    for account in account_list:
        account_dict[account['securitiesAccount']['accountId']] = account
    
    for account in order_data['orders']:
        account_data = account_dict[account['accountNumber']]
        # Account purchase power
        account_available_funds = account_data['securitiesAccount']['projectedBalances']['availableFundsNonMarginableTrade']
        temp_funds = account_available_funds
        for stock in account['stocks']:
            if (datetime.strptime(stock['date'] + " 23:59:59", '%Y-%m-%d %H:%M:%S') < datetime.today()):
                print(f"order date must not be a past date for ticker {stock['tickerCode']}")
                stock['tickerCode'] = ''
                continue
            if (not stock['stock_quantity']):
                quotes = trading_robot.session.get_quotes(instruments=list([stock['tickerCode']]))
                price_of_stock = quotes[stock['tickerCode']]['askPrice']
                if (stock['stock_buying_amount_from_account']):
                    if (temp_funds > int(stock['stock_buying_amount_from_account'])):
                        stock['stock_quantity'] = int(stock['stock_buying_amount_from_account'])//price_of_stock
                    else:
                         stock['stock_quantity'] = int(temp_funds//price_of_stock)
                elif (stock['stock_buying_percentage_from_account']):
                    amount_for_stock = account_available_funds * 0.01 * int(stock['stock_buying_percentage_from_account'])
                    if (temp_funds > amount_for_stock):
                        stock['stock_quantity'] =  amount_for_stock//price_of_stock
                    else:
                        stock['stock_quantity'] = temp_funds//price_of_stock
                else:
                    continue
                temp_funds = temp_funds - (int(stock['stock_quantity']) * price_of_stock)
          #  print(stock)

    return order_data

#This is main call for Limit-order sell scheduling
# Read json file data 
order_data = get_order_json_data()
order_data = validate_data(order_data)

# Initilize scheduler
sched = BlockingScheduler()

# Iterate stocks from order.json
for account in order_data['orders']:
    stocks_list = account['stocks']
    for stocks in stocks_list:
        if (stocks['tickerCode'] == ''):
            continue
        sched_dict = {'account_num': account['accountNumber'], 'stocks_info': stocks }

        sched_obj = json.dumps(sched_dict)
        if (stocks['time'].strip()):
           dateTime = stocks['date'] + ' ' +  stocks['time']
           validate_print_schd_object(sched_obj)
           sched.add_job(schedule_job, 'date', run_date=dateTime, args=[sched_obj])
           #print(dateTime)
        else:
            sched.add_job(schedule_job, 'date', args=[sched_obj])

# Scheduler start
try:
    sched.start()
except (KeyboardInterrupt, SystemExit):
    print("exiting...")
    sched.shutdown()


