import pprint

from datetime import datetime
from datetime import timedelta
from configparser import ConfigParser
from pyrobot.robot import PyRobot
import json
import schedule
import apscheduler.job


def exec_order(sched: dict, buy_obj: dict):
    # Grab configuration values.
    config = ConfigParser()
    config.read('configs/config.ini')

    CLIENT_ID = config.get('main', 'CLIENT_ID')
    REDIRECT_URI = config.get('main', 'REDIRECT_URI')
    CREDENTIALS_PATH = config.get('main', 'JSON_PATH')

    # Read json data
    stocks_data = buy_obj['stocks_info']
    
    stocks_data['partial_fill_retry'] = stocks_data.get('partial_fill_retry', 0)          
    stocks_data['partial_fill_wait_time'] = stocks_data.get('partial_fill_wait_time', 0)          
    stocks_data['lmt_price'] = stocks_data.get('lmt_price', 0)          
    stocks_data['enter_exit_percentage'] = stocks_data.get('enter_exit_percentage', 1) 
    stocks_data['lmt_order_step'] = stocks_data.get('lmt_order_step', 0) 
    stocks_data['lmt_order_step_size'] = stocks_data.get('lmt_order_step_size', 0.25) 


    if (stocks_data['strategy_name'] == 'enter_exit'):
        print("Strategy Name                : Enter and Exit")
        
        if (stocks_data['tickerDesc'] != ""):
            print(f"Stock name              : {stocks_data['tickerDesc']}")

        if (stocks_data['strategy_name'] != ""):
            print(f"Stock ticker            : {stocks_data['tickerCode']}")
        else:
            print("Stocker ticker is mandatory for strategy")
            return
        
        if (stocks_data['asset_type'] != ""):
            print(f"Stock type              : {stocks_data['asset_type']}")
        else:
            print("Assert type is mandatory for the strategy")
            return

        if (stocks_data['date'] != ""):
            print(f"Stock buy/sell date     : {stocks_data['date']}")
        else:
            print("Assert type is mandatory for the strategy")
            return
          
        if (stocks_data['time'] != ""):
            print(f"Stock buy/sell time     : {stocks_data['time']}")
        else:
            print("Stock buy/sell time      : Now")

        if (stocks_data['stock_quantity'] != ""):
            print(f"Stock quantity          : {stocks_data['stock_quantity']}")
        else:
            print("Stock quantity is NULL")
            return

        if (stocks_data['enter_exit_percentage'] != ""):
            print(f"Stock exit profit %     : {stocks_data['enter_exit_percentage']}")
        else:
            print("Stock enter_exit_percentage is NULL")
            return

        if (stocks_data['order_type'] == "lmt"):
            stocks_data['partial_fill_retry'] = stocks_data.get('partial_fill_retry', 0)          
            print(f"Partial retry           : {stocks_data['partial_fill_retry']}")
            
            stocks_data['partial_fill_wait_time'] = stocks_data.get('partial_fill_wait_time', 0)          
            print(f"Partial fill wait-time  : {stocks_data['partial_fill_wait_time']}")

    elif (stocks_data['strategy_name'] == 'enter'):
        print("Strategy Name: Enter - Buying stocks")
        if (stocks_data['strategy_name'] != ""):
            print(f"Stock ticker: {stocks_data['tickerCode']}")
        else:
            print("Stocker ticker is mandatory for strategy")
            return
        
        if (stocks_data['asset_type'] != ""):
            print(f"Stock ticker            : {stocks_data['asset_type']}")
        else:
            print("Assert type is mandatory for the strategy")
            return

        if (stocks_data['date'] != ""):
            print(f"Stock buy/sell date     : {stocks_data['date']}")
        else:
            print("Assert type is mandatory for the strategy")
            return
          
        if (stocks_data['time'] != ""):
            print(f"Stock buy/sell time     : {stocks_data['date']}")
        else:
            print("Stock buy/sell time      : Now")

        if (stocks_data['stock_quantity'] != ""):
            print(f"Stock quantity : {stocks_data['stock_quantity']}")
        else:
            print("Stock quantity is NULL")
            return
    elif (stocks_data['strategy_name'] == 'exit'):
        print("Strategy Name: Enter - Buying stocks")
        if (stocks_data['strategy_name'] != ""):
            print(f"Stock ticker: {stocks_data['tickerCode']}")
        else:
            print("Stocker ticker is mandatory for strategy")
            return
        
        if (stocks_data['asset_type'] != ""):
            print(f"Stock ticker: {stocks_data['asset_type']}")
        else:
            print("Assert type is mandatory for the strategy")
            return

        if (stocks_data['date'] != ""):
            print(f"Stock buy/sell date     : {stocks_data['date']}")
        else:
            print("Assert type is mandatory for the strategy")
            return
          
        if (stocks_data['time'] != ""):
            print(f"Stock buy/sell time     : {stocks_data['date']}")
        else:
            print("Stock buy/sell time      : Now")

        if (stocks_data['stock_quantity'] != ""):
            print(f"Stock quantity          : {stocks_data['stock_quantity']}")
        else:
            print("Stock quantity is NULL")
            return

    stocks_data["enter_or_exit"] = stocks_data['strategy_name']
    stocks_data["quantity"] = stocks_data['stock_quantity']

    # Initalize the robot.
    trading_robot = PyRobot(
        client_id=CLIENT_ID,
        redirect_uri=REDIRECT_URI,
        credentials_path=CREDENTIALS_PATH,
        trading_account=buy_obj['account_num'],
        paper_trading=False
    )

    # Create a Portfolio
    trading_robot_portfolio = trading_robot.create_portfolio()
    
    # Add a positions to portfolio
    position = trading_robot_portfolio.add_pre_earning_positions(position=stocks_data)
    pprint.pprint(trading_robot_portfolio.positions)
    
    if (position):
        trade_dic = {   "order_type":stocks_data['order_type'],
                        "enter_or_exit":stocks_data['enter_or_exit'],
                        "long_or_short":stocks_data['long_or_short'],
                        "partial_fill_retry": stocks_data['partial_fill_retry'],
                        "partial_fill_wait_time": stocks_data['partial_fill_wait_time'],
                        "lmt_price":stocks_data['lmt_price'],
                        "enter_exit_percentage":stocks_data['enter_exit_percentage'],
                        "lmt_order_step": stocks_data['lmt_order_step'],
                        "lmt_order_step_size": stocks_data['lmt_order_step_size']
                    }
        pprint.pprint(trading_robot.trade_stocks(position, trade_dic))